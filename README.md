## https://hinigatsu.gitlab.io/hinigatsu_dot/

**Professional site for developer, focused on web development.**

Made with simplicity in mind, the goal of this project is to be as light and accessible as possible (with the idea of using as less JavaScript possible).
